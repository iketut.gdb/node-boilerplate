import mongoose from 'mongoose';
import express from 'express';
import http from 'http';
import swaggerUi from 'swagger-ui-express';
import swaggerDocs from './utils/SwaggerUtils';
import errorHandler from './middlewares/ErrorHandler';
import indexRoutes from './routes';
import loggerHandler from './middlewares/LoggerHandler';
import checkToken from './middlewares/CheckToken';

require('dotenv').config();

const app = express();

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs, { explorer: true }));
app.use(express.json(), loggerHandler(app), checkToken);

indexRoutes(app);

app.use(errorHandler);

// Connect to DB
mongoose.connect(
  process.env.DB_CONNECTION,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
);
// Listening
const port = process.env.PORT;
const server = http.createServer(app);

server.listen(port);

const shutdownProcess = () => {
  // eslint-disable-next-line no-console
  server.close(() => {
    // eslint-disable-next-line no-console
    process.exit(0);
  });
};

process.on('SIGTERM', shutdownProcess);
process.on('SIGINT', shutdownProcess);

export default app;
