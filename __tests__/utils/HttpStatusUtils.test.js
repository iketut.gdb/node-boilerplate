import {
  isHttpStatusOK,
  isHttpStatusCreated,
  isHttpStatusAccepted,
  isHttpStatusNotFound,
  isHttpStatusForbidden,
  isHttpStatusUnauthorized,
  isHttpStatusBadRequest,
  isHttpStatusConflict,
  isHttpStatusInternalServerError,
  isHttpStatusServiceUnavailable
} from '../../src/utils/HttpStatusUtils';

describe('HttpStatusUtils', () => {
  describe('#isHttpStatusOK', () => {
    it('should return true when status is 200', () => {
      const givenStatus = 200;

      const actualResult = isHttpStatusOK(givenStatus);

      expect(actualResult).toBe(true);
    });

    it('should return false when status is 404', () => {
      const givenStatus = 404;

      const actualResult = isHttpStatusOK(givenStatus);

      expect(actualResult).toBe(false);
    });
  });

  describe('#isHttpStatusCreated', () => {
    it('should return true when status is 201', () => {
      const givenStatus = 201;

      const actualResult = isHttpStatusCreated(givenStatus);

      expect(actualResult).toBe(true);
    });

    it('should return false when status is 200', () => {
      const givenStatus = 200;

      const actualResult = isHttpStatusCreated(givenStatus);

      expect(actualResult).toBe(false);
    });
  });

  describe('#isHttpStatusAccepted', () => {
    it('should return true when status is 202', () => {
      const givenStatus = 202;

      const actualResult = isHttpStatusAccepted(givenStatus);

      expect(actualResult).toBe(true);
    });

    it('should return false when status is 404', () => {
      const givenStatus = 404;

      const actualResult = isHttpStatusAccepted(givenStatus);

      expect(actualResult).toBe(false);
    });
  });

  describe('#isHttpStatusConflict', () => {
    it('should return true when the status is 409', () => {
      const status = 409;

      const actual = isHttpStatusConflict(status);

      expect(actual).toBe(true);
    });

    it('should return false when the status is 200', () => {
      const status = 200;

      const actual = isHttpStatusConflict(status);

      expect(actual).toBe(false);
    });
  });

  describe('#isHttpStatusNotFound', () => {
    it('should return true when status is 404', () => {
      const givenStatus = 404;

      const actualResult = isHttpStatusNotFound(givenStatus);

      expect(actualResult).toBe(true);
    });

    it('should return false when status is 200', () => {
      const givenStatus = 200;

      const actualResult = isHttpStatusNotFound(givenStatus);

      expect(actualResult).toBe(false);
    });
  });

  describe('#isHttpStatusForbidden', () => {
    it('should return true when status is 403', () => {
      const givenStatus = 403;

      const actualResult = isHttpStatusForbidden(givenStatus);

      expect(actualResult).toBe(true);
    });

    it('should return false when status is 200', () => {
      const givenStatus = 200;

      const actualResult = isHttpStatusForbidden(givenStatus);

      expect(actualResult).toBe(false);
    });
  });

  describe('#isHttpStatusUnauthorized', () => {
    it('should return true when status is 401', () => {
      const givenStatus = 401;

      const actualResult = isHttpStatusUnauthorized(givenStatus);

      expect(actualResult).toBe(true);
    });

    it('should return false when status is 200', () => {
      const givenStatus = 200;

      const actualResult = isHttpStatusUnauthorized(givenStatus);

      expect(actualResult).toBe(false);
    });
  });

  describe('#isHttpStatusBadRequest', () => {
    it('should return true when status is 400', () => {
      const givenStatus = 400;

      const actualResult = isHttpStatusBadRequest(givenStatus);

      expect(actualResult).toBe(true);
    });

    it('should return false when status is 200', () => {
      const givenStatus = 200;

      const actualResult = isHttpStatusBadRequest(givenStatus);

      expect(actualResult).toBe(false);
    });
  });

  describe('#isHttpStatusInternalServerError', () => {
    it('should return true when status is 500', () => {
      const givenStatus = 500;

      const actualResult = isHttpStatusInternalServerError(givenStatus);

      expect(actualResult).toBe(true);
    });

    it('should return false when status is 200', () => {
      const givenStatus = 200;

      const actualResult = isHttpStatusInternalServerError(givenStatus);

      expect(actualResult).toBe(false);
    });
  });

  describe('#isHttpStatusServiceUnavailable', () => {
    it('should return true when status is 503', () => {
      const givenStatus = 503;

      const actualResult = isHttpStatusServiceUnavailable(givenStatus);

      expect(actualResult).toBe(true);
    });

    it('should return false when status is 200', () => {
      const givenStatus = 200;

      const actualResult = isHttpStatusServiceUnavailable(givenStatus);

      expect(actualResult).toBe(false);
    });
  });
});
